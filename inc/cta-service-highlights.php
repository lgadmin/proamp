<div class="service-highlights-wrapper">
<div class="img-overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2>Service Highlights</h2>
				<div class="service-highlights">
					
					<a class="service-highlights-item" href="/residential/">
						<i class="fa fa-home fa-5x" aria-hidden="true"></i>
						<p>Residential renovations</p>
					</a>

					<a class="service-highlights-item" href="/commercial/">
						<i class="fa fa-building fa-5x" aria-hidden="true"></i>
						<p>Commercial renovations</p>
					</a>

<!-- 					<a class="service-highlights-item" href="#">
						<i class="fa fa-battery-half fa-5x" aria-hidden="true"></i>
						<p><code>UPS systems and service</code></p>
					</a> -->

					<a class="service-highlights-item" href="/dc-electric-batteries/">
						<i class="fa fa-calendar-o fa-5x" aria-hidden="true"></i>
						<p>DC / battery services</p>
					</a>

					<a class="service-highlights-item" href="/residential/smoke-detector-repair-replacement/">
						<i class="fa fa-free-code-camp fa-5x" aria-hidden="true"></i>
						<p>Smoke detectors and fire alarm systems</p>
					</a>

					<a class="service-highlights-item" href="/residential/lighting-repair-lighting-installation/">
						<i class="fa fa-lightbulb-o fa-5x" aria-hidden="true"></i>
						<p>Lighting</p>
					</a>

					<a class="service-highlights-item" href="/electrical-troubleshooting-repair/">
						<i class="fa fa-list-ol fa-5x" aria-hidden="true"></i>
						<p>Troubleshooting and repairs</p>
					</a>

					<a class="service-highlights-item" href="/residential/home-electrical-safety-inspection/">
						<i class="fa fa-check-square-o fa-5x" aria-hidden="true"></i>
						<p>Home inspections</p>
					</a>

					<a class="service-highlights-item" href="/commercial/data-network-wiring/">
						<i class="fa fa-server fa-5x" aria-hidden="true"></i>
						<p>Network wiring</p>
					</a>

					<a class="service-highlights-item" href="/residential/electrical-service-upgrades/">
						<i class="fa fa-plug fa-5x" aria-hidden="true"></i>
						<p>Electrical service upgrades</p>
					</a>

				</div>
			</div>
		</div>
	</div>	
</div>