<?php
/**
 * proamp functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package proamp
 */

if ( ! function_exists( 'proamp_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function proamp_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on proamp, use a find and replace
	 * to change 'proamp' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'proamp', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'custom-logo' );

	// Menus
	function stonealpha_menus() {
	  register_nav_menus(
	    array(
	      'primary-menu' => __( 'Primary' ),
	      'footer-menu' => __( 'Footer' ),
	      'footer-menu-alt' => __( 'Footer-Alt' )
	    )
	  );
	}
	add_action( 'init', 'stonealpha_menus' );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'proamp_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'proamp_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function proamp_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'proamp_content_width', 640 );
}
add_action( 'after_setup_theme', 'proamp_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function proamp_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'proamp' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'proamp' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Front-Page', 'proamp' ),
		'id'            => 'front-page-1',
		'description'   => esc_html__( 'Add widgets here.', 'proamp' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'proamp_widgets_init' );



/**
 * Enqueue scripts and styles.
 */
function proamp_scripts() {
	// wp_register_style('fontawesome', get_template_directory_uri() . '/fonts/navigation.js' );

	// styles. Maybe register them first, not sure yet.
	wp_enqueue_style( 'proamp-style', get_stylesheet_uri() );

	// Fonts
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i', false );

	// Register Scripts
	// wp_register_script( 'alpha-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_register_script( 'proamp-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_register_script( 'bootstrapjs', get_template_directory_uri() . "/js/bootstrap.min.js", array('jquery'), '3.3.7', true );
	wp_register_script( 'slick-carousel', get_template_directory_uri() . "/js/slick.min.js", array('jquery'), '1.6', true );
	wp_register_script( 'proamp-js', get_template_directory_uri() . "/js/proamp-scripts.js", array('jquery'), false, true );

	// Enqueue scripts. Only load what script(s) are needed. Conditional statement if wish.
	// wp_enqueue_script( 'alpha-navigation' );
	wp_enqueue_script( 'proamp-skip-link-focus-fix' );
	wp_enqueue_script( 'bootstrapjs' );
	wp_enqueue_script( 'slick-carousel' );
	wp_enqueue_script( 'proamp-js' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}
add_action( 'wp_enqueue_scripts', 'proamp_scripts' );


/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function proamp_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'proamp_pingback_header' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Additional features to allow styling of the templates.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
// require get_template_directory() . '/inc/jetpack.php';

// Register Custom Navigation Walker
require_once(get_template_directory() . '/inc/wp-bootstrap-navwalker.php');

/*Remove jquery migrate js*/
function remove_jquery_migrate($scripts)
{
	if (!is_admin() && isset($scripts->registered['jquery'])) {
		$script = $scripts->registered['jquery'];

		if ($script->deps) { // Check whether the script has any dependencies
			$script->deps = array_diff($script->deps, array(
				'jquery-migrate'
			));
		}
	}
}
add_action('wp_default_scripts', 'remove_jquery_migrate');

//Remove Gutenburg css
add_action( 'wp_print_styles', 'wps_deregister_gutenberg_styles', 100 );
function wps_deregister_gutenberg_styles() {
	wp_dequeue_style( 'wp-block-library' );
}