<?php if ( has_post_thumbnail() ) : ?>
	<div class="featured-image">
		<div class="title-cont">
			<span class="featured-title"><?php echo get_the_title(); ?></span>
		</div>
		<div class="img-cont">
			<?php the_post_thumbnail(); ?>
		</div>
	</div>
<?php else : ?>
	<div class="featured-image">
		<div class="title-cont">
			<span class="featured-title"><?php echo get_the_title(); ?></span>
		</div>
		<div class="img-cont">
			 <img src="<?php echo get_stylesheet_directory_uri() . '/images/shutterstock_370043891.jpg'; ?>" alt="electrical wiring">
		</div>
	</div>
<?php endif ?>