<?php $home_title = get_the_title( get_option('page_on_front') ); ?>
<nav class="navbar navbar-inverse">
  <div class="container nav-cont">

      <?php wp_nav_menu( array(
    		'theme_location'    => 'primary-menu',
    		'depth'             => 2,
    		'container'         => 'div',
    		'container_class'   => 'collapse navbar-collapse',
    		'container_id'      => 'main-navbar',
    		'menu_class'        => 'nav navbar-nav',
    		'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
    		'walker'            => new wp_bootstrap_navwalker())
      ); ?>

  </div>
</nav>
