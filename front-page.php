<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package proamp
 */

get_header(); ?>
	
<?php get_template_part("/inc/slick-home"); ?>

<div id="content" class="site-content">
	
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

</div><!-- #content -->

<div class="container">
	<div class="row">
		<section class="col-xs-12 col-sm-6">
			<?php get_template_part("/inc/cta-residential-service"); ?>
		</section>

		<section class="col-xs-12 col-sm-6">
			<?php get_template_part("/inc/cta-commercial-service"); ?>
		</section>
	</div>
</div>

			<?php get_template_part("/inc/cta-service-highlights"); ?>

<div class="container mt-lg mb-xs">
	<div class="row">
		
		<section class="col-sm-12">
			<?php dynamic_sidebar( 'front-page-1' ); ?>
			<hr>
		</section>
	
	</div>
</div>
<?php
// get_sidebar();
get_footer();
