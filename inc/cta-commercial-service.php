<div class="cta cta-alpha">
	<a href="/commercial/">
		<div class="cta-img">
			<img src="<?php echo get_stylesheet_directory_uri() . '/images/shutterstock_238420948.jpg'; ?>"  alt="Office with custom lighting">
		</div>
		<div class="footer text-center"><span class="h1 text-uppercase">Commercial Services</span></div>
	</a>

	<div class="text-center mt-sm cta-alpha-body">
		<p class="">Our uniformed employees are accustomed to working in business environments where professional conduct, tidiness, and minimal disruption is expected.</p> 
		<p>From small simple jobs to large complex projects, contact ProAmp Electric for prompt and reliable service.</p>
		<a href="/commercial/" class="btn btn-primary mt-sm">View commercial services</a>
	</div>
</div>
