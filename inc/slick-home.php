<div class="slidewrap">
	
	<section class="main-slider"> 
		<?php if( have_rows('slides') ): ?>
			<?php while( have_rows('slides') ): the_row(); ?>
				<?php  
					$text = get_sub_field('slide_text');
					$url = get_sub_field('slide_link');
				?>
				<div>
					<?php echo wp_get_attachment_image(get_sub_field('slide_image'), 'full'); ?>
					<?php if ($text || $url): ?>
					<div class="slider-text">
						<?php if ($text): ?>
							<?php echo $text; ?>
						<?php endif ?>
						<?php if ($url): ?>
							<a class="btn btn-primary" href="<?php echo $url; ?>">Learn More</a>
						<?php endif ?>
					</div>
					<?php endif ?>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</section>
	
	<div class="slider-content">
		<div class="text-center header-form-text">
			<h2 class="hidden-xs">Industry Experts.</h2>
			<p>30 years of electrical experience. <br> All electrical work guaranteed.</p>
			<a href="/contact/" class="btn btn-lg btn-primary visible-xs-inline-block mt-sm">Free Quote</a>
		</div>

		<div class="hidden-xs"><?php echo do_shortcode('[ninja_form id=4]'); ?></div>
	</div>

</div>