<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package proamp
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
	
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-20618750-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga  = document.createElement('script');
	    ga.type = 'text/javascript'; ga.async = true;
	    ga.src  = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s   = document.getElementsByTagName('script')[0];
	    s.parentNode.insertBefore(ga, s);
	  })();

	</script>

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'proamp' ); ?></a>

	<div class="hidden-xs"><?php get_template_part("/inc/utility-bar"); ?></div>

	<header id="masthead" class="site-header container-fluid" role="banner">

		<div class="row pt-xs pb-xs">
			

			<div class="site-branding col-sm-3 col-md-3 col-lg-4">
				<?php echo has_custom_logo() ? get_custom_logo() : do_shortcode('[lg-site-logo]'); ?>
			</div>

			<div class="col-sm-9 col-md-9 col-lg-8 text-right">

				<a href="tel:+16044606683" class="btn btn-alpha">
					<i class="fa fa-mobile fa-3x pull-left" aria-hidden="true"></i>
					<span>Give us a call <br> <strong>604-460-6683</strong></span>
				</a>

			
				<a href="mailto:info@proampelectric.ca" class="btn btn-alpha">
					<i class="fa fa-envelope-o fa-3x pull-left" aria-hidden="true"></i>
					<span>Send us <br class="visible-xs"> an email <br> <strong class="hidden-xs">info@proampelectric.ca</strong></span>
				</a>
				
			</div>

		</div>



	</header><!-- #masthead -->

	<?php  get_template_part("/inc/nav-main"); ?>

	
