<div class="well">
	<address>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-12 mb-sm">
				<?php echo has_custom_logo() ? get_custom_logo() : do_shortcode('[lg-site-logo]'); ?>

				<span>
					<span>2231 Hartley Ave</span><br>
					<span>Coquitlam</span>, <span>BC</span>&nbsp;<span>V3K 6W9</span><br>
				</span>
			</div>


			<div class="col-xs-12 col-sm-6 col-md-12">
				<strong>Phone: </strong><span>604-460-6683</span><br>
				<strong>Fax: </strong><span>604-460-1517</span><br>
				<a href="mailto:info@proampelectric.ca">info@proampelectric.ca</a>
			</div>

		</div>
	</address>
</div>
