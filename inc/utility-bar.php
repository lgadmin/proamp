	<div class="container-fluid utility-bar">
		<section class="row">
			
				<div class="col-xs-12 col-sm-4">
					<ul class="nav nav-pills utility-bar-tagline">
						<li role="presentation" ><a href="/contact/">Serving Greater Vancouver & the Fraser Valley</a></li>
					</ul>
				</div>

				<div class="col-xs-12 col-sm-8 text-right">
					<ul class="nav nav-pills pull-right">
						<li role="presentation"><a href="/about-us/">Meet The Owners</a></li>
						<li role="presentation"><a href="/customer-experience/">Customer Experience</a></li>
						<li role="presentation"><a href="https://www.facebook.com/ProAmpElectric/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li role="presentation"><a href="https://twitter.com/ProampLtd" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li role="presentation"><a href="https://www.google.com/maps/place/ProAmp+Electric+Ltd./@49.226039,-122.8349897,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x39388942604904fd!8m2!3d49.226039!4d-122.832801?hl=en" target="_blank"><i class="fa fa-google" aria-hidden="true"></i></a></li>
					</ul>
				</div>

		</section>
	</div>

