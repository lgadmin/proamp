<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package proamp
 */

get_header(); ?>

<?php get_template_part('inc/featured-image') ?>


<div id="content" class="site-content">

	<?php get_template_part('inc/breadcrumb') ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="container py-4">
					<?php  $gallery_title = get_field("gallery_title"); ?>
				<?php if ($gallery_title): ?>
					<?php echo $gallery_title; ?>
				<?php endif; ?>
				<?php 
				$images = get_field('image_gallery');
				$size = 'full'; // (thumbnail, medium, large, full or custom size)
				if( $images ): ?>
					<div class="gallery-grid row">
					    <?php foreach( $images as $image ): ?>
					    	<?php  
					    		$image_caption = $image['caption'];
					    	?>
					        <div class="grid-item col-xs-12 col-sm-4 col-md-3">
					            <a data-lightbox="glass-railing" data-title="<?php echo $image_caption; ?>" href="<?php echo $image['url']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></a>
					            <p class="text-center gallery-caption"><?php echo $image_caption; ?></p>
					        </div>
					    <?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- #content -->
<?php
//  get_sidebar();
get_footer();
