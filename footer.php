<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package proamp
 */

?>

	

	<section class="well-alpha">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<?php get_template_part("/inc/icon-list"); ?>
				</div>
			</div>
		</div>
	</section>

	<footer id="colophon" class="site-footer" role="contentinfo">
		
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-3 col-md-3 col-lg-2 col-lg-offset-1">
					<?php get_template_part("/inc/nav-footer"); ?>
				</div>
			

				<div class="col-sm-9 col-md-9 col-lg-9">
					<article>
						<div class="row">
							<div class="col-sm-12 col-md-5 col-lg-6">
								<h2 class="menu-title-noanchor">Homeowners TRUST ProAmp Electric.</h2>
								<p>ProAmp Electric electricians are guaranteed to be professional and certified to get your home electrical project done right the first time.</p>
								<p>Our unique consultative approach has helped thousands of homeowners in the Greater Vancouver Area enjoy the peace of mind and comforts of reliable electrical work and upgrades.</p>
								<p>Our company has a solid reputation for quality and craftsmanship. We take pride in providing the best electrical services in the market today. Let our electricians light up your project.</p>
							</div>
							<div class="col-sm-12 col-md-4 col-lg-3">
								<?php get_template_part("/inc/address-card"); ?>
							</div>
							<div class="col-md-2 col-lg-2 hidden-xs hidden-sm">
								<div class="myoffsetimage">
									<img src="<?php echo get_stylesheet_directory_uri() . '/images/shutterstock_415006663.jpg'; ?>" alt="Electrician with equipment" class="img-responsive">
								</div>
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>

	
		<div class="container-fluid site-info">
			<div class="row">
				<div class="col-sm-6">
					<?php get_template_part("/inc/site-footer-info"); ?>
				</div>
				<div class="col-sm-6 text-right">
					<?php get_template_part("/inc/site-footer-longevity"); ?>
				</div>
			</div>
		</div>


	</footer><!-- #colophon -->
</div><!-- #page -->

	<script>
		/* <![CDATA[ */
		var google_conversion_id = 970578460;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
	/* ]]> */
	</script>

	<noscript>
		<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src=""//googleads.g.doubleclick.net/pagead/viewthroughconversion/970578460/?value=0&amp;guid=ON&amp;script=0""/> 
		</div>
	</noscript>

<?php wp_footer(); ?>

</body>
</html>
