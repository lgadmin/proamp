<div class="cta cta-alpha">
	
	<a href="/residential/">
		<div class="cta-img">
			<img src="<?php echo get_stylesheet_directory_uri() . '/images/shutterstock_527880745.jpg'; ?>"  alt="Kitchen with special lighting">
		</div>
		<div class="footer text-center"><span class="h1 text-uppercase">Residential Services</span></div>
	</a>

	<div class="text-center mt-sm cta-alpha-body">
		<p class="">At ProAmp, we believe that a residential electrician should treat the homes and families of their clients with the same respect and care as they would their own.</p> 
		<p>Our top priorities are your safety and your satisfaction, and we will address any question or concern to keep you informed and to put your mind at ease.</p>
		<a href="/residential/" class="btn btn-primary mt-sm">View residential services</a>
	</div>
</div>