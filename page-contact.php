<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package proamp
 */

get_header(); ?>

<?php get_template_part('inc/featured-image') ?>


<div id="content" class="site-content">

	<?php get_template_part('inc/breadcrumb') ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- #content -->

<div class="container">
	<div class="row contact-wrapper">
		<div class="col-sm-12 col-md-8">
			<div class="contact-page contact-page-form well clearfix"><?php echo do_shortcode('[ninja_form id=4]'); ?></div>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php get_template_part("/inc/address-card"); ?>
			<?php if (get_field("office_image")): ?>
				<div class="contact-office-image">
					<img src="<?php echo get_field('office_image')['url']; ?>" alt="<?php echo get_field('office_image')['alt']; ?>">
				</div>
			<?php endif ?>
		</div>
	</div>
</div>
<?php
//  get_sidebar();
get_footer();
